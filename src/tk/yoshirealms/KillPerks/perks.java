package tk.yoshirealms.KillPerks;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Created by dragontamerfred on 31-12-16.
 */
public class perks {
    private static HashMap<UUID, List<String>> perks = new HashMap<UUID, List<String>>();
    private static HashMap<UUID, List<String>> unlocked = new HashMap<UUID, List<String>>();
    public static HashMap<OfflinePlayer, Integer> selecting = new HashMap<OfflinePlayer, Integer>();

    /* player - entity.Player
       number - Number from 0 to 2
       perk - A string with the perk name
       perks: jumper, berserker, speed, invis, regen, suicide
     */
    public static void chancePerks(OfflinePlayer player, int number, String perk) {
        List<String> perkList = new ArrayList<String>();
        perkList.add("none");
        perkList.add("none");
        perkList.add("none");
        if (perks.containsKey(player.getUniqueId())) {
            perkList = perks.get(player.getUniqueId());
        }
        perkList.set(number, perk);
        perks.put(player.getUniqueId(), perkList);
    }

    public static List<String> getPerks(OfflinePlayer player) {
        if (perks.containsKey(player.getUniqueId())) {
            return perks.get(player.getUniqueId());
        } else {
            return null;
        }
    }

    public static void setPerks(OfflinePlayer player, List<String> perk) {
        perks.put(player.getUniqueId(), perk);
    }

    public static boolean hasUnlocked(OfflinePlayer player, String type) {
        if (unlocked.containsKey(player.getUniqueId())) {
            return unlocked.get(player.getUniqueId()).contains(type);
        } else {
            return false;
        }
    }

    public static void setUnlocked(OfflinePlayer player, String type, Boolean b) {
        List<String> Uperks;
        if (unlocked.containsKey(player.getUniqueId())) {
            Uperks = unlocked.get(player.getUniqueId());
        } else {
            Uperks = new ArrayList<String>();
        }
        if (b) {
            Uperks.add(type);
        } else if (!(b)) {
            Uperks.remove(type);
        }
        unlocked.put(player.getUniqueId(), Uperks);
    }

    public static boolean hasPerk(OfflinePlayer player, String type) {
        if (perks.containsKey(player.getUniqueId())) {
            if (perks.get(player.getUniqueId()) != null)
            for (String str : perks.get(player.getUniqueId())) {
                if (str.equals(type)) {
                    return true;
                }
            }
        }
        return false;
    }

    static void load() {
        List<String> s = killPerks.config.getStringList("perks");

        for (String str : s) {
            if (str != null) {
                String[] words = str.split(":");
                if (words[1] != null) {
                    String[] SPerks = words[1].split(",");
                    List<String> perkList = new ArrayList<String>();
                    perkList.add(SPerks[0]);
                    perkList.add(SPerks[1]);
                    perkList.add(SPerks[2]);
                    setPerks(Bukkit.getOfflinePlayer(UUID.fromString(words[0])), perkList);
                }
            }
        }
        List<String> s1 = killPerks.config.getStringList("unlocked");
        if (s1.size() >= 1) {
            for (String str : s1) {
                if (str != null) {
                    String[] words = str.split(":");
                    if (words[1] != null) {
                        String[] SPerks = words[1].split(",");
                        for (String s2 : SPerks) {
                            setUnlocked(Bukkit.getOfflinePlayer(UUID.fromString(words[0])), s2, true);
                        }
                    }
                }
            }
        }
    }

    static void save() {
        List<String> s = killPerks.config.getStringList("perks");
        s.clear();
        for (UUID p : perks.keySet()) {
            s.add(p + ":" + perks.get(p).get(0) + "," + perks.get(p).get(1) + "," + perks.get(p).get(2));
        }

        List<String> s1 = killPerks.config.getStringList("unlocked");
        s1.clear();
        for (UUID p : unlocked.keySet()) {
            String s3 = p + ":";
            Boolean b1 = true;
            for (String s2 : unlocked.get(p)) {
                if (b1) {
                    s3 = s3 + s2;
                    b1 = false;
                } else {
                    s3 = s3 + "," + s2;
                }
            }
                s1.add(s3);
        }
        killPerks.config.set("unlocked", s1);
        killPerks.config.set("perks", s);
    }
}
