package tk.yoshirealms.KillPerks;

import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 2-1-17.
 */
public class perkSelectGui {
    private Inventory perkSelectGui = Bukkit.createInventory(null, 54, ChatColor.DARK_GRAY + "Select " + ChatColor.DARK_RED + "Perks");

    private ItemStack glassPane;
    private ItemStack back;
    private ItemStack selection;
    private ItemStack berserker;
    private ItemStack speed;
    private ItemStack jumper;
    private ItemStack regen;
    private ItemStack invis;
    private ItemStack suicide;
    public static int berserkerPrice = 4000;
    public static int speedPrice = 3000;
    public static int jumperPrice = 2500;
    public static int regenPrice = 4500;
    public static int invisPrice = 2000;
    public static int suicidePrice = 5500;

    public void open(Player player, int slot) {

        perks.selecting.put(player, slot-1);

        //random stuff
        short glassPaneDur = 14;
        glassPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, glassPaneDur);
        back = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        selection = new ItemStack(Material.TRIPWIRE_HOOK, 1);

        //perk items
        berserker = new ItemStack(Material.DIAMOND_SWORD, 1);
        speed = new ItemStack(Material.SUGAR, 1);
        jumper = new ItemStack(Material.RABBIT_FOOT, 1);
        regen = new ItemStack(Material.REDSTONE, 1);
        invis = new ItemStack(Material.COAL, 1);
        suicide = new ItemStack(Material.TNT, 1);

        //get ItemMeta
        ItemMeta glassPaneMeta = glassPane.getItemMeta();
        ItemMeta selectionMeta = selection.getItemMeta();
        ItemMeta backMeta = back.getItemMeta();
        ItemMeta berserkerMeta = berserker.getItemMeta();
        ItemMeta speedMeta = speed.getItemMeta();
        ItemMeta jumperMeta = jumper.getItemMeta();
        ItemMeta regenMeta = regen.getItemMeta();
        ItemMeta invisMeta = invis.getItemMeta();
        ItemMeta suicideMeta = suicide.getItemMeta();

        //make lores
        List<String> jumperLore = new ArrayList<String>();
        if (!(perks.hasUnlocked(player, "jumper"))) {
            jumperLore.add(ChatColor.DARK_RED + "Price: " + ChatColor.WHITE + jumperPrice);
        }
        jumperLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        jumperLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "5 seconds of jump boost");
        List<String> speedLore = new ArrayList<String>();
        if (!(perks.hasUnlocked(player, "speed"))) {
            speedLore.add(ChatColor.DARK_RED + "Price: " + ChatColor.WHITE + speedPrice);
        }
        speedLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        speedLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "5 seconds of speed");
        List<String> berserkerLore = new ArrayList<String>();
        if (!(perks.hasUnlocked(player, "berserker"))) {
            berserkerLore.add(ChatColor.DARK_RED + "Price: " + ChatColor.WHITE + berserkerPrice);
        }
        berserkerLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        berserkerLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "5 seconds of strength");
        List<String> regenLore = new ArrayList<String>();
        if (!(perks.hasUnlocked(player, "regen"))) {
            regenLore.add(ChatColor.DARK_RED + "Price: " + ChatColor.WHITE + regenPrice);
        }
        regenLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        regenLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "2 hearts of regen");
        List<String> invisLore = new ArrayList<String>();
        if (!(perks.hasUnlocked(player, "invis"))) {
            invisLore.add(ChatColor.DARK_RED + "Price: " + ChatColor.WHITE + invisPrice);
        }
        invisLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        invisLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "5 seconds of invisibility");
        List<String> suicideLore = new ArrayList<String>();
        if (!(perks.hasUnlocked(player, "suicide"))) {
            suicideLore.add(ChatColor.DARK_RED + "Price: " + ChatColor.WHITE + suicidePrice);
        }
        suicideLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On death");
        suicideLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "You drop a block of tnt");

        //set names
        glassPaneMeta.setDisplayName(" ");
        selectionMeta.setDisplayName(ChatColor.DARK_GRAY + "Perk selection");
        backMeta.setDisplayName(ChatColor.DARK_GRAY + "Back");
        if (perks.hasUnlocked(player, "berserker")) {
            berserkerMeta.setDisplayName(ChatColor.DARK_GREEN + "Berserker");
        } else {
            berserkerMeta.setDisplayName(ChatColor.RED + "Berserker");
        }
        if (perks.hasUnlocked(player, "speed")) {
            speedMeta.setDisplayName(ChatColor.DARK_GREEN + "Speed");
        } else {
            speedMeta.setDisplayName(ChatColor.RED + "Speed");
        }
        if (perks.hasUnlocked(player, "jumper")) {
            jumperMeta.setDisplayName(ChatColor.DARK_GREEN + "Jumper");
        } else {
            jumperMeta.setDisplayName(ChatColor.RED + "Jumper");
        }
        if (perks.hasUnlocked(player, "regen")) {
            regenMeta.setDisplayName(ChatColor.DARK_GREEN + "Regen");
        } else {
            regenMeta.setDisplayName(ChatColor.RED + "Regen");
        }
        if (perks.hasUnlocked(player, "invis")) {
            invisMeta.setDisplayName(ChatColor.DARK_GREEN + "Invis");
        } else {
            invisMeta.setDisplayName(ChatColor.RED + "Invis");
        }
        if (perks.hasUnlocked(player, "suicide")) {
            suicideMeta.setDisplayName(ChatColor.DARK_GREEN + "Suicide Note");
        } else {
            suicideMeta.setDisplayName(ChatColor.RED + "Suicide Note");
        }

        //set lore
        berserkerMeta.setLore(berserkerLore);
        speedMeta.setLore(speedLore);
        jumperMeta.setLore(jumperLore);
        regenMeta.setLore(regenLore);
        invisMeta.setLore(invisLore);
        suicideMeta.setLore(suicideLore);

        //set meta
        glassPane.setItemMeta(glassPaneMeta);
        selection.setItemMeta(selectionMeta);
        back.setItemMeta(backMeta);
        berserker.setItemMeta(berserkerMeta);
        speed.setItemMeta(speedMeta);
        jumper.setItemMeta(jumperMeta);
        regen.setItemMeta(regenMeta);
        invis.setItemMeta(invisMeta);
        suicide.setItemMeta(suicideMeta);

        //Glass Border
        perkSelectGui.setItem(0, glassPane);
        perkSelectGui.setItem(1, glassPane);
        perkSelectGui.setItem(2, glassPane);
        perkSelectGui.setItem(3, glassPane);
        perkSelectGui.setItem(4, glassPane);
        perkSelectGui.setItem(5, glassPane);
        perkSelectGui.setItem(6, glassPane);
        perkSelectGui.setItem(7, glassPane);
        perkSelectGui.setItem(8, glassPane);
        perkSelectGui.setItem(9, glassPane);
        perkSelectGui.setItem(17, glassPane);
        perkSelectGui.setItem(18, glassPane);
        perkSelectGui.setItem(26, glassPane);
        perkSelectGui.setItem(27, glassPane);
        perkSelectGui.setItem(35, glassPane);
        perkSelectGui.setItem(36, glassPane);
        perkSelectGui.setItem(44, glassPane);
        perkSelectGui.setItem(45, glassPane);
        perkSelectGui.setItem(46, glassPane);
        perkSelectGui.setItem(47, glassPane);
        perkSelectGui.setItem(48, glassPane);
        perkSelectGui.setItem(49, glassPane);
        perkSelectGui.setItem(50, glassPane);
        perkSelectGui.setItem(51, glassPane);
        perkSelectGui.setItem(52, glassPane);

        /*
           0  1  2  3  4  5  6  7  8
           9  10 11 12 13 14 15 16 17
           18 19 20 21 22 23 24 25 26
           27 28 29 30 31 32 33 34 35
           36 37 38 39 40 41 42 43 44
           45 46 47 48 49 50 51 52 53
         */

        //random stuff
        perkSelectGui.setItem(53, back);
        perkSelectGui.setItem(13, selection);
        perkSelectGui.setItem(40, selection);

        //perks
        perkSelectGui.setItem(20, berserker);
        perkSelectGui.setItem(21, speed);
        perkSelectGui.setItem(22, jumper);
        perkSelectGui.setItem(23, regen);
        perkSelectGui.setItem(24, invis);
        perkSelectGui.setItem(29, suicide);

        player.openInventory(perkSelectGui);
    }
}
