package tk.yoshirealms.KillPerks.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.yoshirealms.KillPerks.perkGui;

/**
 * Created by dragontamerfred on 1-1-17.
 */
public class commandPerks implements CommandExecutor {

    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            perkGui perkGui = new perkGui();
            perkGui.open(player);
            return true;
        }
        return false;
    }
}
