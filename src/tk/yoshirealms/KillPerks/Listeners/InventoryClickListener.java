package tk.yoshirealms.KillPerks.Listeners;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import tk.yoshirealms.KillPerks.killPerks;
import tk.yoshirealms.KillPerks.perkGui;
import tk.yoshirealms.KillPerks.perkSelectGui;
import tk.yoshirealms.KillPerks.perks;

/**
 * Created by dragontamerfred on 2-1-17.
 */
public class InventoryClickListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();
        int slot = event.getSlot();
        Inventory inv = event.getInventory();
        if (inv.getName().equals(ChatColor.DARK_GRAY + "Selected " + ChatColor.DARK_RED + "Perks")) {
            if (slot == 30) {
                perkSelectGui perkSelectGui = new perkSelectGui();
                perkSelectGui.open(player, 1);
            } else if (slot == 31) {
                perkSelectGui perkSelectGui = new perkSelectGui();
                perkSelectGui.open(player, 2);
            } else if (slot == 32) {
                perkSelectGui perkSelectGui = new perkSelectGui();
                perkSelectGui.open(player, 3);
            } else if (slot == 53) {
                player.closeInventory();
            }
            event.setCancelled(true);
        } else if (inv.getName().equals(ChatColor.DARK_GRAY + "Select " + ChatColor.DARK_RED + "Perks")) {
            if (slot == 20) {
                if (!(perks.hasPerk(player, "berserker"))) {
                    if (perks.hasUnlocked(player, "berserker")) {
                        perks.chancePerks(player, perks.selecting.get(player), "berserker");
                    } else if (killPerks.econ.getBalance(player) >= perkSelectGui.berserkerPrice) {
                        perks.setUnlocked(player, "berserker", true);
                        perks.chancePerks(player, perks.selecting.get(player), "berserker");
                        killPerks.econ.withdrawPlayer(player, perkSelectGui.berserkerPrice);
                    } else {
                        player.sendMessage("§8[§4§lYoshi§6§lRealms§8]:§c You dont have enough money to buy that perk");
                    }
                }
                perkGui perkgui = new perkGui();
                perkgui.open(player);
            } else if (slot == 21) {
                if (!(perks.hasPerk(player, "speed"))) {
                    if (perks.hasUnlocked(player, "speed")) {
                        perks.chancePerks(player, perks.selecting.get(player), "speed");
                    } else if (killPerks.econ.getBalance(player) >= perkSelectGui.speedPrice) {
                        perks.setUnlocked(player, "speed", true);
                        perks.chancePerks(player, perks.selecting.get(player), "speed");
                        killPerks.econ.withdrawPlayer(player, perkSelectGui.speedPrice);
                    } else {
                        player.sendMessage("§8[§4§lYoshi§6§lRealms§8]:§c You dont have enough money to buy that perk");
                    }
                }
                perkGui perkgui = new perkGui();
                perkgui.open(player);
            } else if (slot == 22) {
                if (!(perks.hasPerk(player, "jumper"))) {
                    if (perks.hasUnlocked(player, "jumper")) {
                        perks.chancePerks(player, perks.selecting.get(player), "jumper");
                    } else if (killPerks.econ.getBalance(player) >= perkSelectGui.jumperPrice) {
                        perks.setUnlocked(player, "jumper", true);
                        perks.chancePerks(player, perks.selecting.get(player), "jumper");
                        killPerks.econ.withdrawPlayer(player, perkSelectGui.jumperPrice);
                    } else {
                        player.sendMessage("§8[§4§lYoshi§6§lRealms§8]:§c You dont have enough money to buy that perk");
                    }
                }
                perkGui perkgui = new perkGui();
                perkgui.open(player);
            } else if (slot == 23) {
                if (!(perks.hasPerk(player, "regen"))) {
                    if (perks.hasUnlocked(player, "regen")) {
                        perks.chancePerks(player, perks.selecting.get(player), "regen");
                    } else if (killPerks.econ.getBalance(player) >= perkSelectGui.regenPrice) {
                        perks.setUnlocked(player, "regen", true);
                        perks.chancePerks(player, perks.selecting.get(player), "regen");
                        killPerks.econ.withdrawPlayer(player, perkSelectGui.regenPrice);
                    } else {
                        player.sendMessage("§8[§4§lYoshi§6§lRealms§8]:§c You dont have enough money to buy that perk");
                    }
                }
                perkGui perkgui = new perkGui();
                perkgui.open(player);
            } else if (slot == 24) {
                if (!(perks.hasPerk(player, "invis"))) {
                    if (perks.hasUnlocked(player, "invis")) {
                        perks.chancePerks(player, perks.selecting.get(player), "invis");
                    } else if (killPerks.econ.getBalance(player) >= perkSelectGui.invisPrice) {
                        perks.setUnlocked(player, "invis", true);
                        perks.chancePerks(player, perks.selecting.get(player), "invis");
                        killPerks.econ.withdrawPlayer(player, perkSelectGui.invisPrice);
                    } else {
                        player.sendMessage("§8[§4§lYoshi§6§lRealms§8]:§c You dont have enough money to buy that perk");
                    }
                }
                perkGui perkgui = new perkGui();
                perkgui.open(player);
            } else if (slot == 29) {
                if (!(perks.hasPerk(player, "suicide"))) {
                    if (perks.hasUnlocked(player, "suicide")) {
                        perks.chancePerks(player, perks.selecting.get(player), "suicide");
                    } else if (killPerks.econ.getBalance(player) >= perkSelectGui.suicidePrice) {
                        perks.setUnlocked(player, "suicide", true);
                        perks.chancePerks(player, perks.selecting.get(player), "suicide");
                        killPerks.econ.withdrawPlayer(player, perkSelectGui.suicidePrice);
                    } else {
                        player.sendMessage("§8[§4§lYoshi§6§lRealms§8]:§c You dont have enough money to buy that perk");
                    }
                }
                perkGui perkgui = new perkGui();
                perkgui.open(player);
            } else if (slot == 53) {
                perkGui perkgui = new perkGui();
                perkgui.open(player);
            }
            event.setCancelled(true);
        }
    }
}
