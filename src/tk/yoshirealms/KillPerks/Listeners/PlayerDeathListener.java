package tk.yoshirealms.KillPerks.Listeners;

import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import tk.yoshirealms.KillPerks.killPerks;
import tk.yoshirealms.KillPerks.perks;

import java.util.HashMap;
import java.util.List;

/**
 * Created by dragontamerfred on 30-12-16.
 */
public class PlayerDeathListener implements Listener {

    HashMap<Player, Player> lastHit = new HashMap<Player, Player>();

    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent event) {
        Player dead = event.getEntity();
        List perkD = perks.getPerks(dead);
        if (perkD != null) {
            for (int i = 0; i != perkD.size(); i++) {
                if (perkD.get(i).equals("suicide")) {
                    dead.getWorld().createExplosion(dead.getLocation().getBlockX(), dead.getLocation().getBlockY(), dead.getLocation().getBlockZ(), 2, false, false);
                }
            }
        }
        if (lastHit.containsKey(dead)) {
            Player killer = lastHit.get(dead);
            if (killer != null) {
                EconomyResponse r = killPerks.econ.depositPlayer(killer, 10);
                if (r.transactionSuccess()) {
                    killer.sendMessage("§8[§4§lYoshi§6§lRealms§8]:§c You received $10 for killing " + dead.getName());
                }
                List perk = perks.getPerks(killer);
                if (perk != null) {
                    for (int i = 0; i != perk.size(); i++) {
                        if (perk.get(i).toString().equals("jumper")) {
                            killer.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 100, 0, false, false));
                        } else if (perk.get(i).toString().equals("speed")) {
                            killer.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100, 1, false, false));
                        } else if (perk.get(i).toString().equals("berserker")) {
                            killer.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100, 0, false, false));
                        } else if (perk.get(i).toString().equals("regen")) {
                            killer.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 100, 1, false, false));
                        }
                    }

                }
            }
        }
    }

    @EventHandler
    public void onPlayerDamageEvent(EntityDamageByEntityEvent event) {
        if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
            Player damaged = (Player) event.getEntity();
            Player hitter = (Player) event.getDamager();
            lastHit.put(damaged, hitter);
        }
    }
}
