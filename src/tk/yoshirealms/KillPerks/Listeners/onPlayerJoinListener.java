package tk.yoshirealms.KillPerks.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import tk.yoshirealms.KillPerks.perks;

/**
 * Created by dragontamerfred on 3-1-17.
 */
public class onPlayerJoinListener implements Listener {

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (perks.getPerks(player) != null) {
            for (String str : perks.getPerks(player)) {
                System.out.println(str);
            }
        }
    }
}
