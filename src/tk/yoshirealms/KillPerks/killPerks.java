package tk.yoshirealms.KillPerks;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.configuration.Configuration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import tk.yoshirealms.KillPerks.Listeners.InventoryClickListener;
import tk.yoshirealms.KillPerks.Listeners.PlayerDeathListener;
import tk.yoshirealms.KillPerks.Listeners.onPlayerJoinListener;
import tk.yoshirealms.KillPerks.commands.commandPerks;


/**
 * Created by dragontamerfred on 30-12-16.
 */
public class killPerks extends JavaPlugin {
    public static Economy econ = null;
    public static Configuration config;
    public static killPerks plugin;


    @Override
    public void onEnable() {
        System.out.println(ver.pluginPrefix + "Has started.");
        this.getCommand("perks").setExecutor(new commandPerks());
        getServer().getPluginManager().registerEvents(new PlayerDeathListener(), this);
        getServer().getPluginManager().registerEvents(new InventoryClickListener(), this);
        getServer().getPluginManager().registerEvents(new onPlayerJoinListener(), this);
        setupEconomy();
        config = this.getConfig();
        saveDefaultConfig();
        perks.load();
        plugin = this;
    }

    @Override
    public void onDisable() {
        System.out.println(ver.pluginPrefix + "Has stopped.");
        perks.save();
        this.saveConfig();
    }

    private boolean setupEconomy() {
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return econ != null;
    }
}
