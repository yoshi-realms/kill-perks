package tk.yoshirealms.KillPerks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dragontamerfred on 1-1-17.
 */
public class perkGui implements Listener {

    private Inventory perkGui = Bukkit.createInventory(null, 54, ChatColor.DARK_GRAY + "Selected " + ChatColor.DARK_RED + "Perks");

    private ItemStack perkSlot1;
    private ItemStack perkSlot2;
    private ItemStack perkSlot3;
    private ItemStack glassPane;
    private ItemStack back;
    private ItemStack selections;

    public void open(Player player) {
        short glassPaneDur = 14;
        short zero = 0;
        glassPane = new ItemStack(Material.STAINED_GLASS_PANE, 1, glassPaneDur);
        perkSlot1 = new ItemStack(Material.BARRIER, 1);
        perkSlot2 = new ItemStack(Material.BARRIER, 1);
        perkSlot3 = new ItemStack(Material.BARRIER, 1);
        back = new ItemStack(Material.TRIPWIRE_HOOK, 1);
        selections = new ItemStack(Material.TRIPWIRE_HOOK, 1);

        if (perks.getPerks(player) != null) {
            if (perks.getPerks(player).size() >= 1) {
                perkSlot1.setDurability(zero);
                if (perks.getPerks(player).get(0).toString().equals("jumper"))
                    perkSlot1.setType(Material.RABBIT_FOOT);
                if (perks.getPerks(player).get(0).toString().equals("speed"))
                    perkSlot1.setType(Material.SUGAR);
                if (perks.getPerks(player).get(0).toString().equals("berserker"))
                    perkSlot1.setType(Material.DIAMOND_SWORD);
                if (perks.getPerks(player).get(0).toString().equals("suicide"))
                    perkSlot1.setType(Material.TNT);
                if (perks.getPerks(player).get(0).toString().equals("invis"))
                    perkSlot1.setType(Material.COAL);
                if (perks.getPerks(player).get(0).toString().equals("regen"))
                    perkSlot1.setType(Material.REDSTONE);

                if (perks.getPerks(player).size() >= 2) {
                    perkSlot2.setDurability(zero);
                    if (perks.getPerks(player).get(1).toString().equals("jumper"))
                        perkSlot2.setType(Material.RABBIT_FOOT);
                    if (perks.getPerks(player).get(1).toString().equals("speed"))
                        perkSlot2.setType(Material.SUGAR);
                    if (perks.getPerks(player).get(1).toString().equals("berserker"))
                        perkSlot2.setType(Material.DIAMOND_SWORD);
                    if (perks.getPerks(player).get(1).toString().equals("suicide"))
                        perkSlot2.setType(Material.TNT);
                    if (perks.getPerks(player).get(1).toString().equals("invis"))
                        perkSlot2.setType(Material.COAL);
                    if (perks.getPerks(player).get(1).toString().equals("regen"))
                        perkSlot2.setType(Material.REDSTONE);

                    if (perks.getPerks(player).size() >= 3) {
                        perkSlot3.setDurability(zero);
                        if (perks.getPerks(player).get(2).toString().equals("jumper"))
                            perkSlot3.setType(Material.RABBIT_FOOT);
                        if (perks.getPerks(player).get(2).toString().equals("speed"))
                            perkSlot3.setType(Material.SUGAR);
                        if (perks.getPerks(player).get(2).toString().equals("berserker"))
                            perkSlot3.setType(Material.DIAMOND_SWORD);
                        if (perks.getPerks(player).get(2).toString().equals("suicide"))
                            perkSlot3.setType(Material.TNT);
                        if (perks.getPerks(player).get(2).toString().equals("invis"))
                            perkSlot3.setType(Material.COAL);
                        if (perks.getPerks(player).get(2).toString().equals("regen"))
                            perkSlot3.setType(Material.REDSTONE);
                    }
                }
            }
        }

        //Get Item Meta
        ItemMeta glassPaneMeta = glassPane.getItemMeta();
        ItemMeta perkSlot1Meta = perkSlot1.getItemMeta();
        ItemMeta perkSlot2Meta = perkSlot2.getItemMeta();
        ItemMeta perkSlot3Meta = perkSlot3.getItemMeta();
        ItemMeta backMeta = back.getItemMeta();
        ItemMeta selectionsMeta = selections.getItemMeta();

        //Lores
        List<String> jumperLore = new ArrayList<String>();
        jumperLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        jumperLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "5 seconds of jump boost");
        List<String> speedLore = new ArrayList<String>();
        speedLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        speedLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "5 seconds of speed");
        List<String> berserkerLore = new ArrayList<String>();
        berserkerLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        berserkerLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "3 seconds of strength");
        List<String> regenLore = new ArrayList<String>();
        regenLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        regenLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "2 hearts of regen");
        List<String> invisLore = new ArrayList<String>();
        invisLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On kill");
        invisLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "3 seconds of invisibility");
        List<String> suicideLore = new ArrayList<String>();
        suicideLore.add(ChatColor.GRAY + "Usage: " + ChatColor.RED + "On death");
        suicideLore.add(ChatColor.GRAY + "Effect: " + ChatColor.RED + "You drop a block of tnt");

        //set Display Name
        glassPaneMeta.setDisplayName(" ");
        backMeta.setDisplayName(ChatColor.DARK_GRAY + "Exit");
        selectionsMeta.setDisplayName(ChatColor.DARK_GRAY + "Selections");
        if (perks.getPerks(player) != null) {
            if (perks.getPerks(player).size() >= 1) {
                if (perks.getPerks(player).get(0).equals("jumper")) {
                    perkSlot1Meta.setDisplayName(ChatColor.DARK_GREEN + "Jumper");
                    perkSlot1Meta.setLore(jumperLore);
                } else if (perks.getPerks(player).get(0).equals("speed")) {
                    perkSlot1Meta.setDisplayName(ChatColor.BLUE + "Speed");
                    perkSlot1Meta.setLore(speedLore);
                } else if (perks.getPerks(player).get(0).equals("berserker")) {
                    perkSlot1Meta.setDisplayName(ChatColor.DARK_RED + "Berserker");
                    perkSlot1Meta.setLore(berserkerLore);
                } else if (perks.getPerks(player).get(0).equals("invis")) {
                    perkSlot1Meta.setDisplayName(ChatColor.DARK_GRAY + "Invis");
                    perkSlot1Meta.setLore(invisLore);
                } else if (perks.getPerks(player).get(0).equals("regen")) {
                    perkSlot1Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Regen");
                    perkSlot1Meta.setLore(regenLore);
                } else if (perks.getPerks(player).get(0).equals("suicide")) {
                    perkSlot1Meta.setDisplayName(ChatColor.GRAY + "Suicide Note");
                    perkSlot1Meta.setLore(suicideLore);
                } else {
                    perkSlot1Meta.setDisplayName(ChatColor.GRAY + "Nothing Selected");
                }
            } else {
                perkSlot1Meta.setDisplayName(ChatColor.GRAY + "Nothing Selected");
            }
            if (perks.getPerks(player).size() >= 2) {
                if (perks.getPerks(player).get(1).equals("jumper")) {
                    perkSlot2Meta.setDisplayName(ChatColor.DARK_GREEN + "Jumper");
                    perkSlot2Meta.setLore(jumperLore);
                } else if (perks.getPerks(player).get(1).equals("speed")) {
                    perkSlot2Meta.setDisplayName(ChatColor.BLUE + "Speed");
                    perkSlot2Meta.setLore(speedLore);
                } else if (perks.getPerks(player).get(1).equals("berserker")) {
                    perkSlot2Meta.setDisplayName(ChatColor.DARK_RED + "Berserker");
                    perkSlot2Meta.setLore(berserkerLore);
                } else if (perks.getPerks(player).get(1).equals("invis")) {
                    perkSlot2Meta.setDisplayName(ChatColor.DARK_GRAY + "Invis");
                    perkSlot2Meta.setLore(invisLore);
                } else if (perks.getPerks(player).get(1).equals("regen")) {
                    perkSlot2Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Regen");
                    perkSlot2Meta.setLore(regenLore);
                } else if (perks.getPerks(player).get(1).equals("suicide")) {
                    perkSlot2Meta.setDisplayName(ChatColor.GRAY + "Suicide Note");
                    perkSlot2Meta.setLore(suicideLore);
                } else {
                    perkSlot2Meta.setDisplayName(ChatColor.GRAY + "Nothing Selected");
                }
            } else {
                perkSlot2Meta.setDisplayName(ChatColor.GRAY + "Nothing Selected");
            }
            if (perks.getPerks(player).size() >= 3) {
                if (perks.getPerks(player).get(2).equals("jumper")) {
                    perkSlot3Meta.setDisplayName(ChatColor.DARK_GREEN + "Jumper");
                    perkSlot3Meta.setLore(jumperLore);
                } else if (perks.getPerks(player).get(2).equals("speed")) {
                    perkSlot3Meta.setDisplayName(ChatColor.BLUE + "Speed");
                    perkSlot3Meta.setLore(speedLore);
                } else if (perks.getPerks(player).get(2).equals("berserker")) {
                    perkSlot3Meta.setDisplayName(ChatColor.DARK_RED + "Berserker");
                    perkSlot3Meta.setLore(berserkerLore);
                } else if (perks.getPerks(player).get(2).equals("invis")) {
                    perkSlot3Meta.setDisplayName(ChatColor.DARK_GRAY + "Invis");
                    perkSlot3Meta.setLore(invisLore);
                } else if (perks.getPerks(player).get(2).equals("regen")) {
                    perkSlot3Meta.setDisplayName(ChatColor.LIGHT_PURPLE + "Regen");
                    perkSlot3Meta.setLore(regenLore);
                } else if (perks.getPerks(player).get(2).equals("suicide")) {
                    perkSlot3Meta.setDisplayName(ChatColor.GRAY + "Suicide Note");
                    perkSlot3Meta.setLore(suicideLore);
                } else {
                    perkSlot3Meta.setDisplayName(ChatColor.GRAY + "Nothing Selected");
                }
            } else {
                perkSlot3Meta.setDisplayName(ChatColor.GRAY + "Nothing Selected");
            }
        }

        //set Item meta
        glassPane.setItemMeta(glassPaneMeta);
        back.setItemMeta(backMeta);
        selections.setItemMeta(selectionsMeta);
        perkSlot1.setItemMeta(perkSlot1Meta);
        perkSlot2.setItemMeta(perkSlot2Meta);
        perkSlot3.setItemMeta(perkSlot3Meta);

        //Glass Border
        perkGui.setItem(0, glassPane);
        perkGui.setItem(1, glassPane);
        perkGui.setItem(2, glassPane);
        perkGui.setItem(3, glassPane);
        perkGui.setItem(4, glassPane);
        perkGui.setItem(5, glassPane);
        perkGui.setItem(6, glassPane);
        perkGui.setItem(7, glassPane);
        perkGui.setItem(8, glassPane);
        perkGui.setItem(9, glassPane);
        perkGui.setItem(17, glassPane);
        perkGui.setItem(18, glassPane);
        perkGui.setItem(26, glassPane);
        perkGui.setItem(27, glassPane);
        perkGui.setItem(35, glassPane);
        perkGui.setItem(36, glassPane);
        perkGui.setItem(44, glassPane);
        perkGui.setItem(45, glassPane);
        perkGui.setItem(46, glassPane);
        perkGui.setItem(47, glassPane);
        perkGui.setItem(48, glassPane);
        perkGui.setItem(49, glassPane);
        perkGui.setItem(50, glassPane);
        perkGui.setItem(51, glassPane);
        perkGui.setItem(52, glassPane);

        //random stuff
        perkGui.setItem(53, back);
        perkGui.setItem(22, selections);

        //perk Slots
        perkGui.setItem(30, perkSlot1);
        perkGui.setItem(31, perkSlot2);
        perkGui.setItem(32, perkSlot3);

        /*
           0  1  2  3  4  5  6  7  8
           9  10 11 12 13 14 15 16 17
           18 19 20 21 22 23 24 25 26
           27 28 29 30 31 32 33 34 35
           36 37 38 39 40 41 42 43 44
           45 46 47 48 49 50 51 52 53
         */

        player.openInventory(perkGui);
    }
}
